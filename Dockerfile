FROM openjdk:8-jdk-alpine

WORKDIR /app

VOLUME /tmp

ENV SPRING_DATASOURCE_URL=jdbc:postgresql://db:5432/itjapanesedb

ENV SPRING_DATASOURCE_USERNAME=postgres

ENV SPRING_DATASOURCE_PASSWORD=crquan07

ENV SPRING_JPA_HIBERNATE_DDL_AUTO=update

COPY /target/itjapanese-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java","-jar","./app.jar"]

